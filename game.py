x_dim=10
y_dim=15
field = list()
for x in range(0, x_dim):
    column = list()
    field.append(column)
    for y in range(0, y_dim):
        column.append(' ')

for y in range(y_dim):
    field[y%x_dim][y] = str(y)

while True:
    x = int(input("Enter x:"))
    y = int(input("Enter y:"))
    char = input("Enter char:")
    field[y][x] = char
    
    print ('_'*(x_dim+2))
    for y in range(y_dim):
        row_str = '|'
        for x in range (x_dim):
            row_str = row_str+field[x][y]
        row_str=row_str+'|'
        print (row_str)
    print('_'*(x_dim+2))
